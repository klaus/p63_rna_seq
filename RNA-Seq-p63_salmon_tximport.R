## ----style, echo=FALSE, results="asis", cache=FALSE----------------------
library(knitr)
options(digits=3, width=80)
golden_ratio <- (1 + sqrt(5)) / 2
opts_chunk$set(echo=TRUE,tidy=FALSE,include=TRUE,
               dev=c('png', 'pdf', 'svg'), fig.height = 5, fig.width = 4 * golden_ratio, comment = '  ', dpi = 300,
cache = TRUE)

## ---- echo=FALSE, cache=FALSE--------------------------------------------
print(date())

## ----setup, cache = FALSE------------------------------------------------
library("biomaRt")
library("knitr")
library("org.Hs.eg.db")
library("BiocStyle")
library("geneplotter")
library("LSD")
library("DESeq2")
library("gplots")
library("RColorBrewer")
library("stringr")
library("topGO")
library("genefilter")
library("magrittr")
library("EDASeq")
library("fdrtool")
library("Rmixmod")
library("forcats")
library("Biostrings")
library("readxl")
library("purrr")
library("jsonlite")
library("ensembldb")
library("tximport")
library("tidyverse")
library("ggrepel")
library("plotly")
library("ggthemes")
fastq_dir <-  file.path("/g/scb/patil/klaus/rawDataElli")

# directory for annotation data, transcriptome etc
data_dir <- "/home/klaus/data"

## ----get_ensembl_90, eval=TRUE, cache=FALSE------------------------------

library(AnnotationHub)
## Load the annotation resource.
ah <- AnnotationHub()

## Query for all available EnsDb databases
query(ah, "EnsDb")

## search for ENSEMBL 90 Mus Musculus database
mm_Db <- query(ah, pattern = c("Mus Musculus", "EnsDb", 90))
## What have we got
mm_Db

## it has the AH ID "AH57770"
mm_90 <- mm_Db[["AH57770"]]

#save(mm_90, file = "mm_90.RData", compress = "xz")

## ----merge_ensembl_fast, eval=FALSE--------------------------------------
## load("data_p63/mm_90.RData")
## ens_fasta_dir <- file.path("/home/klaus/data")
## list.files(ens_fasta_dir)
## 
## cdna <- readDNAStringSet(file.path(ens_fasta_dir,
##                                    "Mus_musculus.GRCm38.cdna.all.fa.gz"))
## 
## ncrna <- readDNAStringSet(file.path(ens_fasta_dir,
##                                    "Mus_musculus.GRCm38.ncrna.fa.gz"))
## 
## 
## get_anno <- function(trans_fasta){
## 
## 
##   annot_list <- str_split(names(trans_fasta), pattern = "[ :]", n = 18)
## 
##   data_idx <- c(1, 5, 6, 7, 8, 10, 12, 14, 16, 18)
## 
##   anno_names <- c("transcript_id", "chromosome", "start", "end", "strand", "gene",
##                  "gene_biotype", "transcript_biotype", "gene_symbol",
##                  "description")
## 
##   anno  <- map(annot_list, ~ .x[data_idx] )
##   names(anno)  <- map(anno, 1)
## 
## 
##   anno_df <- matrix(NA, nrow = length(anno), ncol = length(anno_names))
## 
##   for(i in seq_len(length(anno))){
## 
##     anno_df[i, ] <- anno[[i]]
## 
##   }
##   colnames(anno_df) <- anno_names
##   anno_df <- as_data_frame(anno_df)
##   anno_df
## 
## }
## 
## 
## 
## anno_cdna <- get_anno(cdna)
## anno_ncrna <- get_anno(ncrna)
## 
## # check overlap (there should be none)
## 
## ovrlpp <- semi_join(anno_cdna, anno_ncrna,
##                     by = c("transcript_id" = "transcript_id"))
## 
## table(anno_cdna$transcript_id %in% anno_ncrna$transcript_id)
## 
## anno_trans <- bind_rows(anno_cdna, anno_ncrna)
## 
## 
## writeXStringSet(x = c(cdna, ncrna),
##                 filepath = file.path(data_dir,
##                 "ens_90_mm_cdna_ncrna.fa"), compress = FALSE)
## 
## anno_trans <- mutate(anno_trans,
##                      tx = str_extract(transcript_id, "([[:alnum:]]+)"),
##                      gn = str_extract(gene, "([[:alnum:]]+)"))
## 
## save(anno_trans, file = "anno_trans.RData")

## ----load_data, eval=FALSE-----------------------------------------------
## fastQFiles <- list.files(fastq_dir, pattern = "^RNA_")
## fastQFiles
## 
## splittedFastQ <- str_split(fastQFiles, "[_.]")
## 
## metadata <- data.frame(fastQ = fastQFiles,
##                        condition = sapply(splittedFastQ, "[", 3),
##                        dose = ifelse(sapply(splittedFastQ,
##                                             function(x){x[5]}) == "low",
##                                             "low", "high" ),
##                        id = sapply(splittedFastQ,
##                                           function(x){
##                                             paste0(x[1:4], collapse = "__")})
## 
##                        )
## metadata
## save(metadata, file = "metadata.RData")

## ----1h_6h_samples_and_others, dependson="load_data", eval= TRUE---------
load("metadata.RData")
metadata <- subset(metadata,  (dose == "high" | condition == "wt"))

metadata <- arrange(metadata, desc(condition) )

# reorder according to the condition 
metadata <- metadata[c(1:4,10:5),]
metadata
metadata$condition



## ----salmon_index, eval=FALSE--------------------------------------------
## #template from Charlotte Soneson
## 
## salm_idx <- paste0("salmon index -t " ,
##                    file.path(data_dir, "ens_90_mm_cdna_ncrna.fa"),
##                    " -i ",
##                    file.path(data_dir,
##                              "ens_90_mm_cdna_ncrna_sal_0.8.2.sidx" ),
##                   " --type quasi -k 15 -p 4")
## 
## 
## sink(file = "salmon_index.sh", type="output")
## cat('#!/bin/sh \n\n')
## cat(salm_idx)
## sink()

## ----salm_quant----------------------------------------------------------

quantify_salmon <- function(rtype, files, salmondir, smp, salmonbin = "salmon", 
                            libtype, salmonindex, bias = FALSE) {
  # if (!any(file.exists(c(paste0(salmondir, "/", smp, "/aux_info/meta_info.json"),
  #                        paste0(salmondir, "/", smp, "/aux/meta_info.json"))))) {
    if (rtype == "single") {
      salmon <- sprintf("%s quant -p 6 -l %s -i %s -r  %s -o %s %s ",
                        salmonbin, 
                        libtype,
                        salmonindex,
                        files, 
                        paste0(salmondir, "/", smp),
                        ifelse(bias, "--seqBias", ""))
      return(salmon)
    } else  {
      salmon <- sprintf("%s quant -p 6 -l %s -i %s -1  %s -2 <(cat %s) -o %s %s ",
                        salmonbin, 
                        libtype,
                        salmonindex,
                        files$f1,
                        files$f2,
                        paste0(salmondir, "/", smp),
                        ifelse(bias, "--seqBias", ""))
      
      return(salmon)
    } 
  
}

## -------------------------------------------------------------------------- ##
##                                Run Salmon                                  ##
## -------------------------------------------------------------------------- ##
#' Quantify transcript abundance with Salmon
#' 
#' @param rtype "single" or "paired"
#' @param files Names of the fastq files to use for the quantification
#' @param smp Sample ID
#' @param salmonbin Path to Salmon binary
#' @param libtype The \code{LIBTYPE} argument passed to Salmon
#' @param index Path to Salmon index
#' @param bias Whether or not to use the --seqBias argument of Salmon  
#' 
#' @return Returns nothing, but generates Salmon output files in the
#'   salmondir/smp directory.
#  


## ----quant_salmon, eval=FALSE--------------------------------------------
## 
## salm_idx <- file.path(data_dir, "ens_90_mm_cdna_ncrna_sal_0.8.2.sidx" )
## 
## 
## salm_cmds  <- metadata  %>%
##            do( cmd= paste0(quantify_salmon(rtype = "single",
##                                   files = file.path(fastq_dir,.$fastQ),
##                                   smp = .$id,
##                                   libtype = "U",
##                                   salmonindex = salm_idx,
##                                   bias = TRUE,
##                                   salmondir = file.path(getwd(), "slm_out"),
##                                   ), " &>> salmon_log.md ")
##            )
## 
## 
##   sink(file = "salmon_commands_high_dose.sh", type="output")
##   cat('#!/bin/sh \n\n')
##   walk(salm_cmds$cmd, ~ cat(paste0(.x, "\n\n")))
##   sink()
## 
## 

## ----create_new_metadata-------------------------------------------------
load("anno_trans.RData")



tx2gene <- anno_trans %>%  select(transcript_id, gene)

# group_by(tx2gene, gn, gene) %>% summarize(nnn = n()) %>%
#                          filter (nnn > 1)

al_stats <- function(sl_meta_files){
  
  map_df(map(sl_meta_files, read_json), ~ tibble(al_rate = .x$percent_mapped,
                          total_reads = .x$num_processed,
                          mapped_reads = .x$num_mapped))
}

metadata <- metadata %>%
              mutate(slm_quant = 
                       paste0(file.path(getwd(), "slm_out"), "/", id, "/quant.sf"),
                     slm_info = 
                        paste0(file.path(getwd(), "slm_out"), "/", id, 
                               "/aux_info/meta_info.json"))
# alignment statistics:
add_column(al_stats(metadata$slm_info), metadata$id)

stopifnot(all(file.exists(metadata$slm_quant)))
stopifnot(all(file.exists(metadata$slm_info)))

txi <- tximport(metadata$slm_quant, type = "salmon",
                       tx2gene = tx2gene)

colnames(txi$counts) <- rownames(metadata) <- metadata$id
metadata$condition %<>% as_factor(metadata$condition) 

dds_high <- DESeqDataSetFromTximport(txi, metadata, ~condition)

## ---- non_zero_counts,  dependson="sample_Table_for_DESeq"---------------
gene_counts <- counts(dds_high)
idx.nz <- apply(gene_counts, 1, function(x) { all(x > 0)})
sum(idx.nz)

### random sample from the count matrix
nz.counts <- subset(gene_counts, idx.nz)
sam <- sample(dim(nz.counts)[1], 5)
nz.counts[sam, ]

## ----getSizeFactors,  dependson="sample_Table_for_DESeq"-----------------
dds_high <- estimateSizeFactors(dds_high)
sort(apply(normalizationFactors(dds_high), 2, psych::geometric.mean))

## ----plotDensities, dependson="getSizeFactors"---------------------------
multidensity( log2(counts(dds_high, normalized = TRUE))[idx.nz ,],
    xlab="mean counts", xlim=c(0, 20), main = "densities on log2 scale")

dds_high <-  DESeq(dds_high, betaPrior = TRUE)

## ----helper_function_pca-------------------------------------------------



compute_pca <- function(data_mat, ntop = 500, ...){
  
  pvars <- rowVars(data_mat)
  select <- order(pvars, decreasing = TRUE)[seq_len(min(ntop,
                                                        length(pvars)))]
  
  
  PCA <- prcomp(t(data_mat)[, select], center = TRUE, scale. = FALSE)
  percentVar <- round(100*PCA$sdev^2/sum(PCA$sdev^2),1)
  
  
  return(list(pca = data.frame(PC1 = PCA$x[,1], PC2 = PCA$x[,2],
                      PC3 = PCA$x[,3], PC4 = PCA$x[,4], ...),
              perc_var = percentVar,
              selected_vars = select))

}




## ----sample_heatmaps, fig.cap= "sample heatmaps", fig.height = 20, fig.width = 20----
rld <- rlogTransformation(dds_high, blind=TRUE)

distsRL <- dist(t(assay(rld)))
mat <- as.matrix(distsRL)
rownames(mat) <-  colData(rld)$condition
colnames(mat) <-  colData(rld)$id


## remove diagonal to increase the contrast

diag(mat) <- NA

hmcol <- colorRampPalette(brewer.pal(9, "Blues"))(255)
heatmap.2(mat, trace="none", col = rev(hmcol), margin=c(13, 13))


## ----PCA_computations, dependson= c("sample_heatmaps", "sample_Table_for_DESeq")----
 pca <- compute_pca(assay(rld), 
             condition = colData(rld)$condition,
             sample = colData(rld)$id )

sd_ratio <- sqrt(pca$perc_var[2] / pca$perc_var[1])

pca_plot <- ggplot(pca$pca, aes(x = PC1,  y = PC2,
                color =  condition, label = sample)) +
       geom_point(size = 4) +
       geom_label_repel() +
       ggtitle("PC1 vs PC2, top variable genes") +
       labs(x = paste0("PC1, VarExp:", round(pca$perc_var[1],4)),
       y = paste0("PC2, VarExp:", round(pca$perc_var[2],4))) +
       coord_fixed(ratio = sd_ratio) +
       scale_colour_brewer(palette="Dark2")
       
pca_plot

## ----DESEq2_Results_1h---------------------------------------------------

resultsNames(dds_high)
DESeq2Res1hvsWT <- results(dds_high, 
                         contrast = c("condition", "1h", "wt") )
hist(DESeq2Res1hvsWT$pvalue)

DESeq2Res1hvsWT <- DESeq2Res1hvsWT[ !is.na(DESeq2Res1hvsWT$padj), ]
DESeq2Res1hvsWT <- DESeq2Res1hvsWT[ !is.na(DESeq2Res1hvsWT$pvalue), ]
DESeq2Res1hvsWT <- DESeq2Res1hvsWT[, -which(names(DESeq2Res1hvsWT) == "padj")]

FDR.DESeq2Res1hvsWT <- fdrtool(DESeq2Res1hvsWT$stat, statistic= "normal", plot = T)
FDR.DESeq2Res1hvsWT$param[1, "sd"]

DESeq2Res1hvsWT[,"padj"]  <- FDR.DESeq2Res1hvsWT$qval
DESeq2Res1hvsWT[,"pvalue"]  <- FDR.DESeq2Res1hvsWT$pval



head(DESeq2Res1hvsWT )
table(DESeq2Res1hvsWT$padj < 0.1)

hist(DESeq2Res1hvsWT$pvalue, col = "lavender", main = "1h vs wt")
DESeq2::plotMA(DESeq2Res1hvsWT)

sigGenesTable1h <- subset(DESeq2Res1hvsWT, padj < 0.1)

table(abs(sigGenesTable1h$log2FoldChange) > 1)



## ----DESEq2_Results_2h30-------------------------------------------------
DESeq2Res2h30vsWT <- results(dds_high, 
                         contrast = c("condition", "2h30", "wt") )
# hist(DESeq2Res2h30vsWT$pvalue)

DESeq2Res2h30vsWT <- DESeq2Res2h30vsWT[ !is.na(DESeq2Res2h30vsWT$padj), ]
DESeq2Res2h30vsWT <- DESeq2Res2h30vsWT[ !is.na(DESeq2Res2h30vsWT$pvalue), ]
DESeq2Res2h30vsWT <- DESeq2Res2h30vsWT[, -which(names(DESeq2Res2h30vsWT) == "padj")]

FDR.DESeq2Res2h30vsWT <- fdrtool(DESeq2Res2h30vsWT$stat, statistic= "normal", plot = T)
FDR.DESeq2Res2h30vsWT$param[1, "sd"]

DESeq2Res2h30vsWT[,"padj"]  <- FDR.DESeq2Res2h30vsWT$qval
DESeq2Res2h30vsWT[,"pvalue"]  <- FDR.DESeq2Res2h30vsWT$pval



head(DESeq2Res2h30vsWT )
table(DESeq2Res2h30vsWT$padj < 0.1)

hist(DESeq2Res2h30vsWT$pvalue, col = "chocolate1", main = "2h30 vs wt")
DESeq2::plotMA(DESeq2Res2h30vsWT)

sigGenesTable2h30 <- subset(DESeq2Res2h30vsWT, padj < 0.1)

table(abs(sigGenesTable2h30$log2FoldChange) > 1)



## ----DESEq2_Results_4h---------------------------------------------------
DESeq2Res4hvsWT <- results(dds_high, 
                         contrast = c("condition", "4h", "wt") )

DESeq2Res4hvsWT <- DESeq2Res4hvsWT[ !is.na(DESeq2Res4hvsWT$padj), ]
DESeq2Res4hvsWT <- DESeq2Res4hvsWT[ !is.na(DESeq2Res4hvsWT$pvalue), ]
DESeq2Res4hvsWT <- DESeq2Res4hvsWT[, -which(names(DESeq2Res4hvsWT) == "padj")]

FDR.DESeq2Res4hvsWT <- fdrtool(DESeq2Res4hvsWT$stat, statistic= "normal", plot = T)
FDR.DESeq2Res4hvsWT$param[1, "sd"]

DESeq2Res4hvsWT[,"padj"]  <- FDR.DESeq2Res4hvsWT$qval
DESeq2Res4hvsWT[,"pvalue"]  <- FDR.DESeq2Res4hvsWT$pval



head(DESeq2Res4hvsWT )
table(DESeq2Res4hvsWT$padj < 0.1)

hist(DESeq2Res4hvsWT$pvalue, col = "hotpink4", main = "4h vs wt")
DESeq2::plotMA(DESeq2Res4hvsWT)

sigGenesTable4h <- subset(DESeq2Res4hvsWT, padj < 0.1)

table(abs(sigGenesTable4h$log2FoldChange) > 1)



## ----DESEq2_Results_6h, eval=TRUE----------------------------------------
resultsNames(dds_high)
DESeq2Res6hvsWT <- results(dds_high, 
                         contrast = c("condition", "6h", "wt") )

DESeq2Res6hvsWT <- DESeq2Res6hvsWT[ !is.na(DESeq2Res6hvsWT$padj), ]
DESeq2Res6hvsWT <- DESeq2Res6hvsWT[ !is.na(DESeq2Res6hvsWT$pvalue), ]
DESeq2Res6hvsWT <- DESeq2Res6hvsWT[, -which(names(DESeq2Res6hvsWT) == "padj")]

FDR.DESeq2Res6hvsWT <- fdrtool(DESeq2Res6hvsWT$stat, statistic= "normal", plot = T)
FDR.DESeq2Res6hvsWT$param[1, "sd"]

DESeq2Res6hvsWT[,"padj"]  <- FDR.DESeq2Res6hvsWT$qval
DESeq2Res6hvsWT[,"pvalue"]  <- FDR.DESeq2Res6hvsWT$pval


head(DESeq2Res6hvsWT)
table(DESeq2Res6hvsWT$padj < 0.1)

hist(DESeq2Res6hvsWT$pvalue, col = "dodgerblue4", main = "6h vs wt")
DESeq2::plotMA(DESeq2Res6hvsWT)

sigGenesTable6h <- subset(DESeq2Res6hvsWT, padj < 0.1)

table(abs(sigGenesTable6h$log2FoldChange) > 1)

#intersect(rownames(sigGenesTable6h), rownames(sigGenesTable1h))


## ----add_anno_to_res-----------------------------------------------------

add_ensembl_id <- function(X){
   X %>%
   as_data_frame() %>%
   rownames_to_column(var = "gene")

}

sigGenesTable1h  %<>% add_ensembl_id()
sigGenesTable2h30  %<>% add_ensembl_id()
sigGenesTable4h %<>% add_ensembl_id()
sigGenesTable6h %<>% add_ensembl_id()

anno_gene  <- group_by(anno_trans, gene, 
                          gene_biotype, gene_symbol, description, gn) %>%
                 summarize(nn = n()) %>%
                 select(-nn) %>%
                 unique() %>%
                 ungroup()
save(anno_gene, file = "anno_gene.RData")

sigGenes_1h <- left_join(sigGenesTable1h, anno_gene)
sigGenes_2h30 <- left_join(sigGenesTable2h30, anno_gene)
sigGenes_4h <- left_join(sigGenesTable4h, anno_gene)
sigGenes_6h <- left_join(sigGenesTable6h, anno_gene)


sigGenes_1h %<>% arrange(desc(abs(log2FoldChange)), padj)
sigGenes_2h30 %<>% arrange(desc(abs(log2FoldChange)), padj)
sigGenes_4h %<>% arrange(desc(abs(log2FoldChange)), padj)
sigGenes_6h %<>% arrange(desc(abs(log2FoldChange)), padj)

write.csv(sigGenes_1h, file = "sigGenes1h.csv")
write.csv(sigGenes_2h30, file = "sigGenes2h30.csv")
write.csv(sigGenes_4h, file = "sigGenes4h.csv")
write.csv(sigGenes_6h, file = "sigGenes6h.csv")

## ----expCandidates, dependson="add_anno_to_res"--------------------------

can_genes <- unique(unlist(c(dplyr::select(subset(sigGenes_4h, 
                                                 log2FoldChange > 0), gn), dplyr::select(subset(sigGenes_6h,   log2FoldChange > 0), gn))))

write.csv(can_genes, file = "candidate_genes_nov_17.csv")

## ----comp_to_2015--------------------------------------------------------
can_genes_2015 <- read.csv("late_up_genes_2015.csv")$x

length(intersect(can_genes_2015, can_genes)) / length(can_genes_2015)

not_found <- unique(filter(anno_trans, 
                           gn %in% setdiff(can_genes_2015, can_genes))$gene)

res46 <- rbind(DESeq2Res4hvsWT, DESeq2Res6hvsWT)

res46[not_found,]

hist(res46[not_found, "log2FoldChange"])

anno_nf <- filter(anno_trans, gene %in% not_found)

# View(anno_nf)

## ----ubiprot-------------------------------------------------------------
ubiprot_full <- read_excel("ubi_prot_nov_17.xlsx")
ubiprot_mm <- read_excel("ubi_prot_mm_nov_17.xlsx")

ubiprot_mm <- extract(ubiprot_mm, gene, 
                      into = c("symb", "orga"),
                      "([[:alnum:]]+)_([[:alnum:]]+)")

ubiprot_mm$symb %<>% stringr::str_to_title()
ubiprot_mm
ubiprot_can <- filter(anno_gene, gene_symbol %in% ubiprot_mm$symb)

ubiprot_can$source = "ubiprot"


ubiprot_can <- filter(ubiprot_can, gn %in% can_genes)  
ubiprot_can

## ----protein_domains-----------------------------------------------------
hasProteinData(mm_90)
listTables(mm_90)
# listColumns(mm_90)

# get transcripts for candidate genes
domains_trans <- as_data_frame(mcols(transcripts(mm_90, filter = ~ gene_id == can_genes,
                         columns = c("protein_id", 
                                     "protein_domain_id",
                                     "protein_domain_source",
                                     "interpro_accession"))))
domains_trans

interesting_domains <- c("IPR001841", "IPR003613", "IPR000569")
names(interesting_domains) <- c("Zinc finger, RING-type", 
                                "U box", "HECT domain")

domain_candidates <- domains_trans %>% 
                     filter(interpro_accession %in% interesting_domains) %>%
                     group_by(gene_id, interpro_accession) %>%
                     summarize(no_trans = n())

domain_candidates$source <- "domain analysis"

domain_candidates$gn <- domain_candidates$gene_id

## ---- eval=FALSE---------------------------------------------------------
## mart <- useMart(biomart = "ensembl", dataset = "mmusculus_gene_ensembl",
##                 host = "aug2017.archive.ensembl.org")
## listMarts(mart)
## attr_all  <- as_data_frame(listAttributes(mart))
## 
## hom_att <- filter(attr_all, str_detect(description,"Human"))
## 
## # get human homologeous genes
## 
## homology_hs_ens_90 <- getBM(attributes=
##                      c("ensembl_gene_id", hom_att$name),
##                      filter="ensembl_gene_id",
##                      values= anno_gene$gn,
##                      mart=mart)
## 
## go_att <- filter(attr_all, str_detect(description,"GO"))
## 
## go_mm_ens_90 <- getBM(attributes=
##                      c("ensembl_gene_id", go_att$name),
##                      filter="ensembl_gene_id",
##                      values= anno_gene$gn,
##                      mart=mart)
## save(go_mm_ens_90, file = "go_mm_ens_90.RData")
## 
## save(homology_hs_ens_90, file = "homology_hs_ens_90.RData")
## 

## ----query_human_database------------------------------------------------
load("homology_hs_ens_90.RData")

can_genes_hom <- filter(homology_hs_ens_90,
                        ensembl_gene_id %in% can_genes)

# map candidates to uniprot

human_ligases <- read_excel("human_ligases_medvar.xlsx")
names(human_ligases)[5] <- "uniprot"

hs_org <- org.Hs.eg.db

k_hs <- unique(can_genes_hom$hsapiens_homolog_ensembl_gene)
k_hs <- na.exclude(ifelse(k_hs == "", NA, k_hs))

hom_human_uniprot <- mapIds(hs_org, 
                        keys=k_hs,
       column="UNIPROT", keytype="ENSEMBL",
       multiVals="list")

# exclude genes with no protein matches
hom_human_uniprot <- hom_human_uniprot[map_lgl(hom_human_uniprot,
                                               ~ all(!is.na(.x)))]

match_in_human <- inner_join(map_df(hom_human_uniprot,
                                   ~ tibble(uniprot = .x),
                         .id = "gn_hs" ),human_ligases) %>%
                  inner_join(can_genes_hom,
                             by = c("gn_hs" = "hsapiens_homolog_ensembl_gene"))
                 
        
match_in_human$source <- "human_database"

match_in_human$gn <- filter(can_genes_hom, hsapiens_homolog_ensembl_gene %in% match_in_human$gn_hs )$ensembl_gene_id



## ----go_des--------------------------------------------------------------
load("go_mm_ens_90.RData")

description_hits <- filter(anno_gene, gn %in% can_genes) 

description_hits <- description_hits[!is.na(unlist(
                      str_match(description_hits$description, "ubiquitin"))),]

description_hits$source = "gene_description"                           

go_hits <- filter(go_mm_ens_90, ensembl_gene_id %in% can_genes) 

go_hits <- go_hits[!is.na(unlist(str_match(go_hits$name_1006, "ubiquitin"))),]

go_hits$gn  <- go_hits$ensembl_gene_id 

go_hits$source <- "go_term"

text_hits <- unique(full_join(select(description_hits, gn, source), 
                      select(go_hits, gn, source)))

text_hits

## ----all_candidates------------------------------------------------------
tmp <- unique(full_join(select(domain_candidates, gn, source)
                        , select(match_in_human, gn, source)))

ubiquitin_list <- arrange(left_join(unique(full_join(tmp, text_hits)), 
                            anno_gene), gene_symbol)

# ubiquitin_list <- left_join(ubiquitin_list, anno_gene)
count(ubiquitin_list, gn)

ubiquitin_list 

write_excel_csv(ubiquitin_list, path = "ubiquitin_list.csv")

# sanity check: are candidates in the significant genes identified?

filter(sigGenes_4h, gene %in% ubiquitin_list$gene)
write_excel_csv(filter(sigGenes_4h, gene %in% ubiquitin_list$gene), 
                path = "ubi_4h.csv")
filter(sigGenes_6h, gene %in% ubiquitin_list$gene)
write_excel_csv(filter(sigGenes_6h, gene %in% ubiquitin_list$gene), 
                path = "ubi_6h.csv")


## ----plot_exp, message=FALSE, warning=FALSE, fig.wide = FALSE------------
rld_sub <- as.data.frame(assay(rld)[unique(ubiquitin_list$gene), ]) %>%     
              rownames_to_column(var = "ensembl_id")

rld_sub$gene_smbol <- ubiquitin_list$gene_symbol[match(rld_sub$ensembl_id,
                                                       ubiquitin_list$gene)]

rld_sub_gg <- gather(rld_sub, -ensembl_id, -gene_smbol, 
                     key = "samp", value = "log2_expr")
rld_sub_gg$con <- colData(rld)[rld_sub_gg$samp, "condition"]

rld_sub_gg$timepoint <- as.numeric(rld_sub_gg$con)


lll <- levels(rld_sub_gg$con)
names(lll) <- unique(as.numeric(rld_sub_gg$con))

can_plot <- ggplot(rld_sub_gg, aes(x = con,
                                     y = log2_expr, 
                                   color = gene_smbol)) +
                   geom_jitter(height = 0) +
                   geom_smooth(aes(x = timepoint), se = FALSE) +
                  scale_color_brewer(palette = "Paired") +
                  ggtitle("plot of ubiquitin ligase candidates ") 
  
#can_plot

                 # scale_x_discrete("timepoint",  labels = lll,
                 #                   breaks = names(lll))

ggplotly(can_plot)

## ----can_2015------------------------------------------------------------
cc_2017 <- unique(ubiquitin_list$gn)
length(intersect(cc_2017 , can_genes_2015))

can_2105_in_2017 <- left_join(filter(ubiquitin_list, gn %in% intersect(cc_2017 ,
                                         can_genes_2015)), anno_gene)


can_2105_in_2017

unique(can_2105_in_2017$gene_symbol)

## ---- cache=FALSE--------------------------------------------------------
sessionInfo()

