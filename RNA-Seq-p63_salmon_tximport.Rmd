---
title: "p63 RNA--Seq using ENSEMBL 90 (Aug 17), salmon and tximport"
output:
   BiocStyle::html_document:
      toc: true
      highlight: tango
      df_print: paged
      self_contained: true
      code_download: true
---

<!--
library("rmarkdown");render("RNA-Seq-p63_salmon_tximport.Rmd");knitr::purl("RNA-Seq-p63_salmon_tximport.Rmd")
-->


```{r style, echo=FALSE, results="asis", cache=FALSE}
library(knitr)
options(digits=3, width=80)
golden_ratio <- (1 + sqrt(5)) / 2
opts_chunk$set(echo=TRUE,tidy=FALSE,include=TRUE,
               dev=c('png', 'pdf', 'svg'), fig.height = 5, fig.width = 4 * golden_ratio, comment = '  ', dpi = 300,
cache = TRUE)
```



 **LAST UPDATE AT**

```{r, echo=FALSE, cache=FALSE}
print(date())
```


# Preparations

We first set global chunk options and load the
necessary packages and the data.

```{r setup, cache = FALSE}
library("biomaRt")
library("knitr")
library("org.Hs.eg.db")
library("BiocStyle")
library("geneplotter")
library("LSD")
library("DESeq2")
library("gplots")
library("RColorBrewer")
library("stringr")
library("topGO")
library("genefilter")
library("magrittr")
library("EDASeq")
library("fdrtool")
library("Rmixmod")
library("forcats")
library("Biostrings")
library("readxl")
library("purrr")
library("jsonlite")
library("ensembldb")
library("tximport")
library("tidyverse")
library("ggrepel")
library("plotly")
library("ggthemes")
fastq_dir <-  file.path("/g/scb/patil/klaus/rawDataElli")

# directory for annotation data, transcriptome etc
data_dir <- "/home/klaus/data"
```


# Obtaining the ENSEMBL 90 annotation

We use the package `r Biocpkg("ensembldb")` in order to access an ENSEMBL 90
database for mouse as an `EnsDb` object. The database is downloaded using the `r Biocpkg("AnnotationHub")`
package in Bioconductor. 

```{r get_ensembl_90, eval=TRUE, cache=FALSE}

library(AnnotationHub)
## Load the annotation resource.
ah <- AnnotationHub()

## Query for all available EnsDb databases
query(ah, "EnsDb")

## search for ENSEMBL 90 Mus Musculus database
mm_Db <- query(ah, pattern = c("Mus Musculus", "EnsDb", 90))
## What have we got
mm_Db

## it has the AH ID "AH57770"
mm_90 <- mm_Db[["AH57770"]]

#save(mm_90, file = "mm_90.RData", compress = "xz")
```

# Merging the ENSEMBL transcript fasta files

ENSEMBL provides two different fasta files for coding and non--coding 
transcripts, here we discard IDs from the non--coding file that also
appear in the coding part. (There are none here, but there could be some)

We also remove the version number from the identifiers. 

```{r merge_ensembl_fast, eval=FALSE}
load("data_p63/mm_90.RData")
ens_fasta_dir <- file.path("/home/klaus/data")
list.files(ens_fasta_dir)

cdna <- readDNAStringSet(file.path(ens_fasta_dir, 
                                   "Mus_musculus.GRCm38.cdna.all.fa.gz"))

ncrna <- readDNAStringSet(file.path(ens_fasta_dir, 
                                   "Mus_musculus.GRCm38.ncrna.fa.gz"))


get_anno <- function(trans_fasta){
  
  
  annot_list <- str_split(names(trans_fasta), pattern = "[ :]", n = 18)
 
  data_idx <- c(1, 5, 6, 7, 8, 10, 12, 14, 16, 18)
  
  anno_names <- c("transcript_id", "chromosome", "start", "end", "strand", "gene",
                 "gene_biotype", "transcript_biotype", "gene_symbol",
                 "description")
  
  anno  <- map(annot_list, ~ .x[data_idx] )
  names(anno)  <- map(anno, 1)     
  
  
  anno_df <- matrix(NA, nrow = length(anno), ncol = length(anno_names))
  
  for(i in seq_len(length(anno))){
    
    anno_df[i, ] <- anno[[i]]
    
  }
  colnames(anno_df) <- anno_names
  anno_df <- as_data_frame(anno_df)
  anno_df
    
} 



anno_cdna <- get_anno(cdna)
anno_ncrna <- get_anno(ncrna)

# check overlap (there should be none)

ovrlpp <- semi_join(anno_cdna, anno_ncrna, 
                    by = c("transcript_id" = "transcript_id"))

table(anno_cdna$transcript_id %in% anno_ncrna$transcript_id)

anno_trans <- bind_rows(anno_cdna, anno_ncrna)


writeXStringSet(x = c(cdna, ncrna), 
                filepath = file.path(data_dir, 
                "ens_90_mm_cdna_ncrna.fa"), compress = FALSE)

anno_trans <- mutate(anno_trans,
                     tx = str_extract(transcript_id, "([[:alnum:]]+)"),
                     gn = str_extract(gene, "([[:alnum:]]+)")) 

save(anno_trans, file = "anno_trans.RData")
```




# Preprocessing

## create metadata 

We first create a sample metadata table. We only load fastq--files corresponding
to RNA--Seq data by requiring them to start with "RNA".

```{r load_data, eval=FALSE}
fastQFiles <- list.files(fastq_dir, pattern = "^RNA_")
fastQFiles

splittedFastQ <- str_split(fastQFiles, "[_.]")

metadata <- data.frame(fastQ = fastQFiles, 
                       condition = sapply(splittedFastQ, "[", 3),
                       dose = ifelse(sapply(splittedFastQ, 
                                            function(x){x[5]}) == "low", 
                                            "low", "high" ),
                       id = sapply(splittedFastQ,  
                                          function(x){ 
                                            paste0(x[1:4], collapse = "__")})
                                  
                       )
metadata
save(metadata, file = "metadata.RData")
```


## keep all high radiation samples

... apart from the wt, also only keep high radiation samples 

```{r 1h_6h_samples_and_others, dependson="load_data", eval= TRUE}
load("metadata.RData")
metadata <- subset(metadata,  (dose == "high" | condition == "wt"))

metadata <- arrange(metadata, desc(condition) )

# reorder according to the condition 
metadata <- metadata[c(1:4,10:5),]
metadata
metadata$condition


```



## Quantification with Salmon

We use the leightweight mapper [salmon](https://combine-lab.github.io/salmon/)
to quantify the cDNA fastq files directly using ENSEMBL 90 transcriptome. This
is more sensitive and faster than the traditional alginment + counting workflow.
Furthermore, it gives us quantification on the transcript level, which improve
gene level estimates as well. [Soneson et. al.](http://dx.doi.org/10.12688/f1000research.7563.2)

We set the option `k` to 15 as we have quite short reads (35bp)

## create Salmon index

```{r salmon_index, eval=FALSE}
#template from Charlotte Soneson

salm_idx <- paste0("salmon index -t " , 
                   file.path(data_dir, "ens_90_mm_cdna_ncrna.fa"),
                   " -i ",
                   file.path(data_dir,
                             "ens_90_mm_cdna_ncrna_sal_0.8.2.sidx" ),
                  " --type quasi -k 15 -p 4")


sink(file = "salmon_index.sh", type="output")
cat('#!/bin/sh \n\n')
cat(salm_idx)
sink()
```


## create mapping commands

Here we create the mapping commands for salmon. We use an adapted 
[helper function](https://github.com/markrobinsonuzh/conquer/blob/master/00_help_functions.R) from Charlotte Soneson to create the commands.


```{r salm_quant}

quantify_salmon <- function(rtype, files, salmondir, smp, salmonbin = "salmon", 
                            libtype, salmonindex, bias = FALSE) {
  # if (!any(file.exists(c(paste0(salmondir, "/", smp, "/aux_info/meta_info.json"),
  #                        paste0(salmondir, "/", smp, "/aux/meta_info.json"))))) {
    if (rtype == "single") {
      salmon <- sprintf("%s quant -p 6 -l %s -i %s -r  %s -o %s %s ",
                        salmonbin, 
                        libtype,
                        salmonindex,
                        files, 
                        paste0(salmondir, "/", smp),
                        ifelse(bias, "--seqBias", ""))
      return(salmon)
    } else  {
      salmon <- sprintf("%s quant -p 6 -l %s -i %s -1  %s -2 <(cat %s) -o %s %s ",
                        salmonbin, 
                        libtype,
                        salmonindex,
                        files$f1,
                        files$f2,
                        paste0(salmondir, "/", smp),
                        ifelse(bias, "--seqBias", ""))
      
      return(salmon)
    } 
  
}

## -------------------------------------------------------------------------- ##
##                                Run Salmon                                  ##
## -------------------------------------------------------------------------- ##
#' Quantify transcript abundance with Salmon
#' 
#' @param rtype "single" or "paired"
#' @param files Names of the fastq files to use for the quantification
#' @param smp Sample ID
#' @param salmonbin Path to Salmon binary
#' @param libtype The \code{LIBTYPE} argument passed to Salmon
#' @param index Path to Salmon index
#' @param bias Whether or not to use the --seqBias argument of Salmon  
#' 
#' @return Returns nothing, but generates Salmon output files in the
#'   salmondir/smp directory.
#  

```

Here we create the quantification commands for Salmon and specify that we 
have a single end / unstranded library.

```{r quant_salmon, eval=FALSE}

salm_idx <- file.path(data_dir, "ens_90_mm_cdna_ncrna_sal_0.8.2.sidx" )


salm_cmds  <- metadata  %>% 
           do( cmd= paste0(quantify_salmon(rtype = "single", 
                                  files = file.path(fastq_dir,.$fastQ),
                                  smp = .$id,
                                  libtype = "U",
                                  salmonindex = salm_idx,
                                  bias = TRUE,
                                  salmondir = file.path(getwd(), "slm_out"),
                                  ), " &>> salmon_log.md ")
           )
           
                  
  sink(file = "salmon_commands_high_dose.sh", type="output")
  cat('#!/bin/sh \n\n')
  walk(salm_cmds$cmd, ~ cat(paste0(.x, "\n\n")))
  sink()
  
 
```


## Import to DESeq2

We now import the data into DEseq2 using the package `r Biocpkg("tximport")`.
It requires a table mapping transcripts to genes, which we can easily
obtain using our annotation data extracted from the ensembl fastq file.



```{r create_new_metadata}
load("anno_trans.RData")



tx2gene <- anno_trans %>%  select(transcript_id, gene)

# group_by(tx2gene, gn, gene) %>% summarize(nnn = n()) %>%
#                          filter (nnn > 1)

al_stats <- function(sl_meta_files){
  
  map_df(map(sl_meta_files, read_json), ~ tibble(al_rate = .x$percent_mapped,
                          total_reads = .x$num_processed,
                          mapped_reads = .x$num_mapped))
}

metadata <- metadata %>%
              mutate(slm_quant = 
                       paste0(file.path(getwd(), "slm_out"), "/", id, "/quant.sf"),
                     slm_info = 
                        paste0(file.path(getwd(), "slm_out"), "/", id, 
                               "/aux_info/meta_info.json"))
# alignment statistics:
add_column(al_stats(metadata$slm_info), metadata$id)

stopifnot(all(file.exists(metadata$slm_quant)))
stopifnot(all(file.exists(metadata$slm_info)))

txi <- tximport(metadata$slm_quant, type = "salmon",
                       tx2gene = tx2gene)

colnames(txi$counts) <- rownames(metadata) <- metadata$id
metadata$condition %<>% as_factor(metadata$condition) 

dds_high <- DESeqDataSetFromTximport(txi, metadata, ~condition)
```



## Quality control and Normalization of the count data


Having created the DESeq2 object, the 
next step in the workflow is the quality control of the data. Let's check
how many genes we capture by counting the number of genes that have
non--zero counts in all samples.

```{r, non_zero_counts,  dependson="sample_Table_for_DESeq"}
gene_counts <- counts(dds_high)
idx.nz <- apply(gene_counts, 1, function(x) { all(x > 0)})
sum(idx.nz)

### random sample from the count matrix
nz.counts <- subset(gene_counts, idx.nz)
sam <- sample(dim(nz.counts)[1], 5)
nz.counts[sam, ]
```

In a typical RNA-Seq experiment, there  will be at least several thousand genes
that are expressed in all samples. If the number of non--zero genes
is very low, there is usually something wrong with at least some of the 
samples (e.g. the sample preparation was not done properly, or there was some
problem with the sequencing). 

## Normalization 


As different libraries will be sequenced to different depths, offsets are built
in the statistical model of `r Biocpkg("DESeq2")` to ensure that parameters are comparable. Here we use a size factor based library size normalization in 
conjuction with gene specific average transcript length offsets as obtained 
from salmon.

The geometric mean of these factors per sample should give us an idea
of the relative library sizes. 

```{r getSizeFactors,  dependson="sample_Table_for_DESeq"}
dds_high <- estimateSizeFactors(dds_high)
sort(apply(normalizationFactors(dds_high), 2, psych::geometric.mean))
```

We can see that the size of gen2\_wt is quite low, while gen3\_wt has a substantial
library size.

To assess whether the normalization has worked, we  plot the  densities of 
counts for the different samples. Since most of the genes are (heavily) affected 
by the experimental conditions, a succesful normalization will lead to overlapping
densities.

```{r plotDensities, dependson="getSizeFactors"}
multidensity( log2(counts(dds_high, normalized = TRUE))[idx.nz ,],
    xlab="mean counts", xlim=c(0, 20), main = "densities on log2 scale")

dds_high <-  DESeq(dds_high, betaPrior = TRUE)
```



Here we can see that the normalization has worked, although there are some 
deviations for small counts. We also ran the wrapper function `DEseq` to 
obtain all the neccessary values.

# PCA and sample heatmaps

```{r helper_function_pca}



compute_pca <- function(data_mat, ntop = 500, ...){
  
  pvars <- rowVars(data_mat)
  select <- order(pvars, decreasing = TRUE)[seq_len(min(ntop,
                                                        length(pvars)))]
  
  
  PCA <- prcomp(t(data_mat)[, select], center = TRUE, scale. = FALSE)
  percentVar <- round(100*PCA$sdev^2/sum(PCA$sdev^2),1)
  
  
  return(list(pca = data.frame(PC1 = PCA$x[,1], PC2 = PCA$x[,2],
                      PC3 = PCA$x[,3], PC4 = PCA$x[,4], ...),
              perc_var = percentVar,
              selected_vars = select))

}



```



Note the use of the function \Rfunction{t} to transpose the data matrix. 
We need this because dist calculates distances between data rows and our samples 
constitute the columns. We visualize the distances in a heatmap, using the function 
` heatmap.2 ` from the ` r CRANpkg("gplots")` package. 

Note that we have changed the row names of the distance matrix to
contain the genotype  and the column to contain the sample ID, so
that we have all this information in view when looking at the heatmap.

We also remove the diagonal to increase the contrast.

``` {r sample_heatmaps, fig.cap= "sample heatmaps", fig.height = 20, fig.width = 20}
rld <- rlogTransformation(dds_high, blind=TRUE)

distsRL <- dist(t(assay(rld)))
mat <- as.matrix(distsRL)
rownames(mat) <-  colData(rld)$condition
colnames(mat) <-  colData(rld)$id


## remove diagonal to increase the contrast

diag(mat) <- NA

hmcol <- colorRampPalette(brewer.pal(9, "Blues"))(255)
heatmap.2(mat, trace="none", col = rev(hmcol), margin=c(13, 13))

```



Another way to visualize sample--to--sample distances is a principal--components analysis (PCA).
In this ordination method, the data points (i.e., here, the samples) are projected 
onto the 2D plane such that they spread out optimally. 



```{r PCA_computations, dependson= c("sample_heatmaps", "sample_Table_for_DESeq") }
 pca <- compute_pca(assay(rld), 
             condition = colData(rld)$condition,
             sample = colData(rld)$id )

sd_ratio <- sqrt(pca$perc_var[2] / pca$perc_var[1])

pca_plot <- ggplot(pca$pca, aes(x = PC1,  y = PC2,
                color =  condition, label = sample)) +
       geom_point(size = 4) +
       geom_label_repel() +
       ggtitle("PC1 vs PC2, top variable genes") +
       labs(x = paste0("PC1, VarExp:", round(pca$perc_var[1],4)),
       y = paste0("PC2, VarExp:", round(pca$perc_var[2],4))) +
       coord_fixed(ratio = sd_ratio) +
       scale_colour_brewer(palette="Dark2")
       
pca_plot
```


We can see a nice clustering by condition. However, the
4h samples seem to be quite far apart. This is also shown
in the heatmap. Maybe we should remove one of them?




# Perform testing

We can perform the statistical testing for differential expression 
and extract its results.

The p--values look awful and we do not have any 
power, we have to correct them.

### 1h vs WT  

```{r DESEq2_Results_1h}

resultsNames(dds_high)
DESeq2Res1hvsWT <- results(dds_high, 
                         contrast = c("condition", "1h", "wt") )
hist(DESeq2Res1hvsWT$pvalue)

DESeq2Res1hvsWT <- DESeq2Res1hvsWT[ !is.na(DESeq2Res1hvsWT$padj), ]
DESeq2Res1hvsWT <- DESeq2Res1hvsWT[ !is.na(DESeq2Res1hvsWT$pvalue), ]
DESeq2Res1hvsWT <- DESeq2Res1hvsWT[, -which(names(DESeq2Res1hvsWT) == "padj")]

FDR.DESeq2Res1hvsWT <- fdrtool(DESeq2Res1hvsWT$stat, statistic= "normal", plot = T)
FDR.DESeq2Res1hvsWT$param[1, "sd"]

DESeq2Res1hvsWT[,"padj"]  <- FDR.DESeq2Res1hvsWT$qval
DESeq2Res1hvsWT[,"pvalue"]  <- FDR.DESeq2Res1hvsWT$pval



head(DESeq2Res1hvsWT )
table(DESeq2Res1hvsWT$padj < 0.1)

hist(DESeq2Res1hvsWT$pvalue, col = "lavender", main = "1h vs wt")
DESeq2::plotMA(DESeq2Res1hvsWT)

sigGenesTable1h <- subset(DESeq2Res1hvsWT, padj < 0.1)

table(abs(sigGenesTable1h$log2FoldChange) > 1)


```

This looks good, although only a small number of DE genes are detected.



### 2h30 vs WT 

```{r DESEq2_Results_2h30}
DESeq2Res2h30vsWT <- results(dds_high, 
                         contrast = c("condition", "2h30", "wt") )
# hist(DESeq2Res2h30vsWT$pvalue)

DESeq2Res2h30vsWT <- DESeq2Res2h30vsWT[ !is.na(DESeq2Res2h30vsWT$padj), ]
DESeq2Res2h30vsWT <- DESeq2Res2h30vsWT[ !is.na(DESeq2Res2h30vsWT$pvalue), ]
DESeq2Res2h30vsWT <- DESeq2Res2h30vsWT[, -which(names(DESeq2Res2h30vsWT) == "padj")]

FDR.DESeq2Res2h30vsWT <- fdrtool(DESeq2Res2h30vsWT$stat, statistic= "normal", plot = T)
FDR.DESeq2Res2h30vsWT$param[1, "sd"]

DESeq2Res2h30vsWT[,"padj"]  <- FDR.DESeq2Res2h30vsWT$qval
DESeq2Res2h30vsWT[,"pvalue"]  <- FDR.DESeq2Res2h30vsWT$pval



head(DESeq2Res2h30vsWT )
table(DESeq2Res2h30vsWT$padj < 0.1)

hist(DESeq2Res2h30vsWT$pvalue, col = "chocolate1", main = "2h30 vs wt")
DESeq2::plotMA(DESeq2Res2h30vsWT)

sigGenesTable2h30 <- subset(DESeq2Res2h30vsWT, padj < 0.1)

table(abs(sigGenesTable2h30$log2FoldChange) > 1)


```


### 4h vs WT 

```{r DESEq2_Results_4h}
DESeq2Res4hvsWT <- results(dds_high, 
                         contrast = c("condition", "4h", "wt") )

DESeq2Res4hvsWT <- DESeq2Res4hvsWT[ !is.na(DESeq2Res4hvsWT$padj), ]
DESeq2Res4hvsWT <- DESeq2Res4hvsWT[ !is.na(DESeq2Res4hvsWT$pvalue), ]
DESeq2Res4hvsWT <- DESeq2Res4hvsWT[, -which(names(DESeq2Res4hvsWT) == "padj")]

FDR.DESeq2Res4hvsWT <- fdrtool(DESeq2Res4hvsWT$stat, statistic= "normal", plot = T)
FDR.DESeq2Res4hvsWT$param[1, "sd"]

DESeq2Res4hvsWT[,"padj"]  <- FDR.DESeq2Res4hvsWT$qval
DESeq2Res4hvsWT[,"pvalue"]  <- FDR.DESeq2Res4hvsWT$pval



head(DESeq2Res4hvsWT )
table(DESeq2Res4hvsWT$padj < 0.1)

hist(DESeq2Res4hvsWT$pvalue, col = "hotpink4", main = "4h vs wt")
DESeq2::plotMA(DESeq2Res4hvsWT)

sigGenesTable4h <- subset(DESeq2Res4hvsWT, padj < 0.1)

table(abs(sigGenesTable4h$log2FoldChange) > 1)


```




### 6h vs WT  
The number of highly regulated genes increaeses at time point 6.


```{r DESEq2_Results_6h, eval=TRUE}
resultsNames(dds_high)
DESeq2Res6hvsWT <- results(dds_high, 
                         contrast = c("condition", "6h", "wt") )

DESeq2Res6hvsWT <- DESeq2Res6hvsWT[ !is.na(DESeq2Res6hvsWT$padj), ]
DESeq2Res6hvsWT <- DESeq2Res6hvsWT[ !is.na(DESeq2Res6hvsWT$pvalue), ]
DESeq2Res6hvsWT <- DESeq2Res6hvsWT[, -which(names(DESeq2Res6hvsWT) == "padj")]

FDR.DESeq2Res6hvsWT <- fdrtool(DESeq2Res6hvsWT$stat, statistic= "normal", plot = T)
FDR.DESeq2Res6hvsWT$param[1, "sd"]

DESeq2Res6hvsWT[,"padj"]  <- FDR.DESeq2Res6hvsWT$qval
DESeq2Res6hvsWT[,"pvalue"]  <- FDR.DESeq2Res6hvsWT$pval


head(DESeq2Res6hvsWT)
table(DESeq2Res6hvsWT$padj < 0.1)

hist(DESeq2Res6hvsWT$pvalue, col = "dodgerblue4", main = "6h vs wt")
DESeq2::plotMA(DESeq2Res6hvsWT)

sigGenesTable6h <- subset(DESeq2Res6hvsWT, padj < 0.1)

table(abs(sigGenesTable6h$log2FoldChange) > 1)

#intersect(rownames(sigGenesTable6h), rownames(sigGenesTable1h))

```


We now join the annotation to the results tables and export them.


```{r add_anno_to_res}

add_ensembl_id <- function(X){
   X %>%
   as_data_frame() %>%
   rownames_to_column(var = "gene")

}

sigGenesTable1h  %<>% add_ensembl_id()
sigGenesTable2h30  %<>% add_ensembl_id()
sigGenesTable4h %<>% add_ensembl_id()
sigGenesTable6h %<>% add_ensembl_id()

anno_gene  <- group_by(anno_trans, gene, 
                          gene_biotype, gene_symbol, description, gn) %>%
                 summarize(nn = n()) %>%
                 select(-nn) %>%
                 unique() %>%
                 ungroup()
save(anno_gene, file = "anno_gene.RData")

sigGenes_1h <- left_join(sigGenesTable1h, anno_gene)
sigGenes_2h30 <- left_join(sigGenesTable2h30, anno_gene)
sigGenes_4h <- left_join(sigGenesTable4h, anno_gene)
sigGenes_6h <- left_join(sigGenesTable6h, anno_gene)


sigGenes_1h %<>% arrange(desc(abs(log2FoldChange)), padj)
sigGenes_2h30 %<>% arrange(desc(abs(log2FoldChange)), padj)
sigGenes_4h %<>% arrange(desc(abs(log2FoldChange)), padj)
sigGenes_6h %<>% arrange(desc(abs(log2FoldChange)), padj)

write.csv(sigGenes_1h, file = "sigGenes1h.csv")
write.csv(sigGenes_2h30, file = "sigGenes2h30.csv")
write.csv(sigGenes_4h, file = "sigGenes4h.csv")
write.csv(sigGenes_6h, file = "sigGenes6h.csv")
```


## Export candidate genes

We export the genes that are up-regulated at 4h and 6hrs as candidate genes
for p63 binding.

```{r expCandidates, dependson="add_anno_to_res"}

can_genes <- unique(unlist(c(dplyr::select(subset(sigGenes_4h, 
                                                 log2FoldChange > 0), gn), dplyr::select(subset(sigGenes_6h,   log2FoldChange > 0), gn))))

write.csv(can_genes, file = "candidate_genes_nov_17.csv")
```

## Compare to previous candidate gene list from 2015

We now check the overlap with the previous results from 2015.

```{r comp_to_2015}
can_genes_2015 <- read.csv("late_up_genes_2015.csv")$x

length(intersect(can_genes_2015, can_genes)) / length(can_genes_2015)

not_found <- unique(filter(anno_trans, 
                           gn %in% setdiff(can_genes_2015, can_genes))$gene)

res46 <- rbind(DESeq2Res4hvsWT, DESeq2Res6hvsWT)

res46[not_found,]

hist(res46[not_found, "log2FoldChange"])

anno_nf <- filter(anno_trans, gene %in% not_found)

# View(anno_nf)
```

We find more with our new pipeline and have an overlap of 
`r length(intersect(can_genes_2015, can_genes)) / length(can_genes_2015)`
The ones not detected by our new pipeline are mainly gene with small
fold change. Furthermore, there are no genes associated with ubiquitin
, E3 and ligase in the list.

# Gather annotation of E3 ligases in mouse

Here we follow the strategy of the paper [Comprehensive database of human E3 ubiquitin ligases: application to aquaporin-2 regulation](http://dx.doi.org/10.1152/physiolgenomics.00031.2016)
to find E3 ligases.

Specifically we check ubiprot and the RING, HECT, or
U-box domains, as well as gene descriptions and GO terms.

## ubiprot

[ubiprot](http://ubiprot.org.ru/index.php?mode=show_all) is 
an ubiquitination related database but we don't find any of them
in our hitlist.

```{r ubiprot}
ubiprot_full <- read_excel("ubi_prot_nov_17.xlsx")
ubiprot_mm <- read_excel("ubi_prot_mm_nov_17.xlsx")

ubiprot_mm <- extract(ubiprot_mm, gene, 
                      into = c("symb", "orga"),
                      "([[:alnum:]]+)_([[:alnum:]]+)")

ubiprot_mm$symb %<>% stringr::str_to_title()
ubiprot_mm
ubiprot_can <- filter(anno_gene, gene_symbol %in% ubiprot_mm$symb)

ubiprot_can$source = "ubiprot"


ubiprot_can <- filter(ubiprot_can, gn %in% can_genes)  
ubiprot_can
```

## protein domain search

As in the paper we look for RING, HECT, or
U-box domains. From [Ardley and Robinson, 2005](http://essays.biochemistry.org/content/41/15.full)

> The E3s are a large, diverse group of proteins, characterized by one of several defining motifs. These include a HECT (homologous to E6-associated protein C-terminus), RING (really interesting new gene) or U-box (a modified RING motif without the full complement of Zn2+-binding ligands) domain. Whereas HECT E3s have a direct role in catalysis during ubiquitination, RING and U-box E3s facilitate protein ubiquitination. These latter two E3 types act as adaptor-like molecules. They bring an E2 and a substrate into sufficiently close proximity to promote the substrate's ubiquitination. Although many RING-type E3s, such as MDM2 (murine double minute clone 2 oncoprotein) and c-Cbl, can apparently act alone, others are found as components of much larger multi-protein complex

Specifically, we use the following InterPro domains / motifs

* Zinc finger, RING-type (IPR001841): <https://www.ebi.ac.uk/interpro/entry/IPR001841>
* U box domain (IPR003613): <https://www.ebi.ac.uk/interpro/entry/IPR003613>
* HECT domain (IPR000569): <https://www.ebi.ac.uk/interpro/entry/IPR000569>

```{r protein_domains}
hasProteinData(mm_90)
listTables(mm_90)
# listColumns(mm_90)

# get transcripts for candidate genes
domains_trans <- as_data_frame(mcols(transcripts(mm_90, filter = ~ gene_id == can_genes,
                         columns = c("protein_id", 
                                     "protein_domain_id",
                                     "protein_domain_source",
                                     "interpro_accession"))))
domains_trans

interesting_domains <- c("IPR001841", "IPR003613", "IPR000569")
names(interesting_domains) <- c("Zinc finger, RING-type", 
                                "U box", "HECT domain")

domain_candidates <- domains_trans %>% 
                     filter(interpro_accession %in% interesting_domains) %>%
                     group_by(gene_id, interpro_accession) %>%
                     summarize(no_trans = n())

domain_candidates$source <- "domain analysis"

domain_candidates$gn <- domain_candidates$gene_id
```

We find that some of our identified significant genes have a ZINC finger RING type
domain.

## use Medvar et. al. human database

In order to use the Medvar et al database to search for candidates,
we need to map homologeous genes. We use ENSEMBL90 as well here.
We also query the GO terms.

### get homologs and GO terms for genes

```{r, eval=FALSE}
mart <- useMart(biomart = "ensembl", dataset = "mmusculus_gene_ensembl",
                host = "aug2017.archive.ensembl.org")
listMarts(mart)
attr_all  <- as_data_frame(listAttributes(mart))

hom_att <- filter(attr_all, str_detect(description,"Human"))

# get human homologeous genes

homology_hs_ens_90 <- getBM(attributes=
                     c("ensembl_gene_id", hom_att$name),
                     filter="ensembl_gene_id",
                     values= anno_gene$gn,
                     mart=mart)

go_att <- filter(attr_all, str_detect(description,"GO"))

go_mm_ens_90 <- getBM(attributes=
                     c("ensembl_gene_id", go_att$name),
                     filter="ensembl_gene_id",
                     values= anno_gene$gn,
                     mart=mart)
save(go_mm_ens_90, file = "go_mm_ens_90.RData")

save(homology_hs_ens_90, file = "homology_hs_ens_90.RData")

```

## Query the human database

```{r query_human_database}
load("homology_hs_ens_90.RData")

can_genes_hom <- filter(homology_hs_ens_90,
                        ensembl_gene_id %in% can_genes)

# map candidates to uniprot

human_ligases <- read_excel("human_ligases_medvar.xlsx")
names(human_ligases)[5] <- "uniprot"

hs_org <- org.Hs.eg.db

k_hs <- unique(can_genes_hom$hsapiens_homolog_ensembl_gene)
k_hs <- na.exclude(ifelse(k_hs == "", NA, k_hs))

hom_human_uniprot <- mapIds(hs_org, 
                        keys=k_hs,
       column="UNIPROT", keytype="ENSEMBL",
       multiVals="list")

# exclude genes with no protein matches
hom_human_uniprot <- hom_human_uniprot[map_lgl(hom_human_uniprot,
                                               ~ all(!is.na(.x)))]

match_in_human <- inner_join(map_df(hom_human_uniprot,
                                   ~ tibble(uniprot = .x),
                         .id = "gn_hs" ),human_ligases) %>%
                  inner_join(can_genes_hom,
                             by = c("gn_hs" = "hsapiens_homolog_ensembl_gene"))
                 
        
match_in_human$source <- "human_database"

match_in_human$gn <- filter(can_genes_hom, hsapiens_homolog_ensembl_gene %in% match_in_human$gn_hs )$ensembl_gene_id


```

## Check gene descriptions and GO terms

Here we check gene descriptions and go terms for the term "ubiquitin"

```{r go_des}
load("go_mm_ens_90.RData")

description_hits <- filter(anno_gene, gn %in% can_genes) 

description_hits <- description_hits[!is.na(unlist(
                      str_match(description_hits$description, "ubiquitin"))),]

description_hits$source = "gene_description"                           

go_hits <- filter(go_mm_ens_90, ensembl_gene_id %in% can_genes) 

go_hits <- go_hits[!is.na(unlist(str_match(go_hits$name_1006, "ubiquitin"))),]

go_hits$gn  <- go_hits$ensembl_gene_id 

go_hits$source <- "go_term"

text_hits <- unique(full_join(select(description_hits, gn, source), 
                      select(go_hits, gn, source)))

text_hits
```

# Join candidates together

Here, we create a final table with all the candidates. Some genes
have been identified by multiple lines of evidence.

```{r all_candidates}
tmp <- unique(full_join(select(domain_candidates, gn, source)
                        , select(match_in_human, gn, source)))

ubiquitin_list <- arrange(left_join(unique(full_join(tmp, text_hits)), 
                            anno_gene), gene_symbol)

# ubiquitin_list <- left_join(ubiquitin_list, anno_gene)
count(ubiquitin_list, gn)

ubiquitin_list 

write_excel_csv(ubiquitin_list, path = "ubiquitin_list.csv")

# sanity check: are candidates in the significant genes identified?

filter(sigGenes_4h, gene %in% ubiquitin_list$gene)
write_excel_csv(filter(sigGenes_4h, gene %in% ubiquitin_list$gene), 
                path = "ubi_4h.csv")
filter(sigGenes_6h, gene %in% ubiquitin_list$gene)
write_excel_csv(filter(sigGenes_6h, gene %in% ubiquitin_list$gene), 
                path = "ubi_6h.csv")

```


## Plot gene expression of the candidate genes across time

```{r plot_exp, message=FALSE, warning=FALSE, fig.wide = FALSE}
rld_sub <- as.data.frame(assay(rld)[unique(ubiquitin_list$gene), ]) %>%     
              rownames_to_column(var = "ensembl_id")

rld_sub$gene_smbol <- ubiquitin_list$gene_symbol[match(rld_sub$ensembl_id,
                                                       ubiquitin_list$gene)]

rld_sub_gg <- gather(rld_sub, -ensembl_id, -gene_smbol, 
                     key = "samp", value = "log2_expr")
rld_sub_gg$con <- colData(rld)[rld_sub_gg$samp, "condition"]

rld_sub_gg$timepoint <- as.numeric(rld_sub_gg$con)


lll <- levels(rld_sub_gg$con)
names(lll) <- unique(as.numeric(rld_sub_gg$con))

can_plot <- ggplot(rld_sub_gg, aes(x = con,
                                     y = log2_expr, 
                                   color = gene_smbol)) +
                   geom_jitter(height = 0) +
                   geom_smooth(aes(x = timepoint), se = FALSE) +
                  scale_color_brewer(palette = "Paired") +
                  ggtitle("plot of ubiquitin ligase candidates ") 
  
#can_plot

                 # scale_x_discrete("timepoint",  labels = lll,
                 #                   breaks = names(lll))

ggplotly(can_plot)
```

## How many / which of the candidates did we identify in 2015?

```{r can_2015}
cc_2017 <- unique(ubiquitin_list$gn)
length(intersect(cc_2017 , can_genes_2015))

can_2105_in_2017 <- left_join(filter(ubiquitin_list, gn %in% intersect(cc_2017 ,
                                         can_genes_2015)), anno_gene)


can_2105_in_2017

unique(can_2105_in_2017$gene_symbol)
```


<!-- # Gene clustering -->

<!-- We can now produce heatmaps of the fold changes of the DE genes -->
<!-- to cluster them by their profiles -->

<!-- ## Expression clustering -->


<!-- We cluster the expression profile of the DE genes across samples -->

<!-- ```{r exp_clustering, dependson="sample_heatmaps_no_outlier", fig.height= 75, fig.width=15, eval=FALSE} -->
<!-- # betas1h <-  unique(select(sigGenes1h, external_gene_name, log2FoldChange)) -->
<!-- # rownames(betas1h) <- betas1h$external_gene_name -->
<!-- # betas1h$external_gene_name <- NULL -->
<!-- # betas1h <- as.matrix(betas1h) -->

<!-- #rbind(select(sigGenes1h, ensembl_gene_id, log2FoldChange),  -->
<!-- #               select(sigGenes6h, ensembl_gene_id, log2FoldChange))   -->

<!-- # colors <- colorRampPalette( rev(brewer.pal(9, "PuOr")) )(255) -->
<!-- # heatmap.2(betas1h, trace="none", dendrogram="row", -->
<!-- #           Colv=FALSE, col=colors, mar=c(12,8)) -->

<!-- genesToCluster <- unique(c(sigGenes_6h$ensembl_gene_id, -->
<!--                         sigGenes_2h30$ensembl_gene_id, -->
<!--                         sigGenes_4h$ensembl_gene_id, -->
<!--                           sigGenes_1h$ensembl_gene_id)) -->

<!-- desForCl <- as.vector(bmALL[match(genesToCluster,   -->
<!--                                    bmALL$ensembl_gene_id),]$external_gene_name) -->

<!-- #bmALL$ensembl_gene_id -->

<!-- colors <- colorRampPalette( rev(brewer.pal(9, "PuOr")) )(255) -->
<!-- sidecols <- c("grey", "mediumorchid1", "mediumorchid3", -->
<!--               "mediumorchid4", "mediumpurple4")[ colData(rld)$condition ] -->
<!-- mat <- assay(rld)[genesToCluster, ] -->
<!-- mat <- mat - rowMeans(mat) -->
<!-- rownames(mat) <- desForCl -->
<!-- colnames(mat) <- colData(rld)$condition -->
<!-- #pdf("test.pdf") -->
<!-- resCluster <- heatmap.2(mat, tracecol="#303030", col=colors, ColSideColors=sidecols, -->
<!--            scale="row", mar= c(10,10), cexRow = 0.9 + 1/log10(dim(mat)[1]), Colv=FALSE) -->
<!-- #dev.off() -->


<!-- ``` -->


# Session information



```{r, cache=FALSE}
sessionInfo()
```

